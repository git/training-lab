#!/bin/sh
#
# Runtime provisioning script for Vagrant-based training lab setup

# runtime type - arm32 (default) or arm64
RUNTIME=$1
if [ "$RUNTIME"x = ""x ]; then
    RUNTIME=arm32
fi
shift
OPTS=$1

# Location of the runtime files. MANIFEST must exist there, and
# describes what else needs to be downloaded: in the format:
# <FILENAME>:<MODE>:<BYTES>:<SHA256>
# Apart from the MANIFEST itself, all files should be compressed with
# gzip. In each case, the script will download $FILENAME.gz,
# decompress it and then validate the checksum
if [ "$OPTS"x = "localtest"x ]; then
    RUNTIME_DL=http://www.einval.org/share/arm-security/$RUNTIME/
else
    RUNTIME_DL=https://www.einval.com/arm/training-lab/$RUNTIME/
fi

# Abort on errors
set -e
# set -x

# Make sure we have updates applied, and all our needed packages. Kill
# inattended-upgrades if it's running, as it will clash here
killall unattended-upgrade unattended-upgrades || true
apt-get update
apt-get dist-upgrade -y
apt-get install -y qemu-system-arm gcc-aarch64-linux-gnu
echo "Toolchain VM running!"

# Now grab the emulated runtime and start that
cd /vagrant
if [ ! -d runtime ]; then
    mkdir runtime
fi
cd runtime

echo "Checking / downloading files needed for the emulated runtime VM"
# Grab all the files we need, and check they're valid
echo "  Downloading MANIFEST"
wget -nv -O MANIFEST $RUNTIME_DL/MANIFEST 
for LINE in $(cat MANIFEST); do
    FILENAME=$(echo $LINE | awk -F: '{print $1}')
    MODE=$(echo $LINE | awk -F: '{print $2}')
    BYTES=$(echo $LINE | awk -F: '{print $3}')
    SHA=$(echo $LINE | awk -F: '{print $4}')
    DL_NEEDED=1
    
    # Quick and dirty - if the file exists and is the right size,
    # we'll believe it's OK. This will save us downloading a 2G test
    # image every time, but make sure we delete it if people are
    # changing tests!
    echo "  Checking $FILENAME.gz"
    if [ -f $FILENAME ]; then
        SIZE=$(stat -c%s $FILENAME)
        if [ $SIZE = $BYTES ]; then
            DL_NEEDED=0
        fi
    fi

    if [ "$OPTS"x = "nodownload"x ]; then
        "IGNORING DOWNLOAD FOR $FILENAME"
        DL_NEEDED=0
    fi
    if [ $DL_NEEDED = 1 ]; then
        # Grab a compressed version of the file, and extract it as we
        # go
        echo "  Downloading $FILENAME.gz"
        wget -nv -O- $RUNTIME_DL/$FILENAME.gz | gzip -cd > $FILENAME
        SHA_FILE=$(sha256sum $FILENAME | awk '{print $1}')
        if [ $SHA_FILE != $SHA ]; then
            echo "Failed to download $FILENAME.gz correctly. Abort"
            exit 1
        fi
    fi
    chmod $MODE $FILENAME
done

echo "Starting emulated runtime VM for $RUNTIME next"
MACH=$RUNTIME ./start_runtime
